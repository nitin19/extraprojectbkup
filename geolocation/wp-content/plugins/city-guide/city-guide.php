<?php
	/**
	 * @package  Wordpress City Guide Manager
	 */
	/*
		Plugin Name:  City Guide
		Plugin URI: 
		Description: Wordpress City Guide
		Version: 1.0.0
		Author: Binary Data
		Author URI: 
		License: GPLv2 or later
		Text Domain: city_guide
		Domain Path: /lang
	*/
		//////////////// Make sure we don't expose any info if called directly
	if ( !function_exists( 'add_action' ) ) {
		echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
		exit;
	}

	/********
	constants
	*********/
	define( 'CTGID_PATH', plugin_dir_path(__FILE__)   );
	define( 'CTGID_URL', plugin_dir_url( __FILE__ )   );
	define( 'CTGID_VERSION', '1.0' );

	define( 'post_type', 'City Guide' );
	define( 'post_taxonomy', 'State' );
	//show_admin_bar( true );

	/**************
	Include Files
	***************/

	require(CTGID_PATH.'/assist/style_n_scripts.php');

	require(CTGID_PATH.'/city_manager/ct_post_type.php');
	require(CTGID_PATH.'/city_manager/city_taxnomies.php');
	require(CTGID_PATH.'/city_manager/city_guide_page.php');

	require(CTGID_PATH.'/includes/templates/all_city_listing.php');	
	require(CTGID_PATH.'/includes/templates/single_taxnomies.php');

	//require(CTGID_PATH.'/includes/templates/single_template.php');

	/**************
	HOOKS
	***************/

	add_action( 'init', 'city_register_post_type' );
	add_action( 'init', 'city_guide_state' );
	
	register_activation_hook(__FILE__, 'city_guide_page');

	add_shortcode('wp_all_city_listing', 'all_city_listing');
	add_action( 'wp_enqueue_scripts', 'CTGID_style_n_scripts', 100);
	add_filter('template_include', 'single_taxonomy_template');
	//add_filter('single_template', 'single_city_guide');

	//add_filter('single_template', 'single_taxnomies_template');	
	// add_action( 'admin_menu', 'books_manager' );	
	// add_action( "add_meta_boxes", "add_custom_meta_box");
	// add_action( "save_post", "save_custom_meta_box", 10, 3);
	// add_action( 'restrict_manage_posts', 'posts_taxonomy_filter' );	

add_action( 'wp_footer', 'ajax_fetch' );
function ajax_fetch() {
?>
<script type="text/javascript">
/*function fetch(){

    jQuery.ajax({
        url: '<?php //echo admin_url('admin-ajax.php'); ?>',
        type: 'post',
        data: { action: 'data_fetch', keyword: jQuery('#keyword').val() },
        success: function(data) {
            jQuery('#datafetch').html( data );
        }
    //});

}*/
jQuery(document).ready(function(){
	//var template_directory_uri = jQuery('.template_directory_uri').val();
		///-- search brands tabs --///
	jQuery("#keyword").keyup(function(){
		jQuery.ajax({
	        url: '<?php echo admin_url('admin-ajax.php'); ?>',
	        type: 'post',
	        data: { action: 'data_fetch', keyword: jQuery('#keyword').val() },
	        success: function(data) {
	            jQuery('#regionsmap').html( data );
	        }
    });
	});
});
</script>

<?php
}
// the ajax function
add_action('wp_ajax_data_fetch' , 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch','data_fetch');
function data_fetch(){
	global $wpdb;
	$zipcode = trim($_POST['keyword']);
	$url = "https://maps.googleapis.com/maps/api/geocode/json?address={$zipcode}&key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I&country=UK";
	$resp_json = file_get_contents($url);
	$resp = json_decode($resp_json, true);
	//print_r($resp);
	$lat = $resp['results'][0]['geometry']['location']['lat'];
    $long = $resp['results'][0]['geometry']['location']['lng'];

    //$url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $zipcode . "&sensor=true";
    $newurl = "https://maps.googleapis.com/maps/api/geocode/json?latlng={$lat},{$long}&key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I&country=UK";

    $address_info = file_get_contents($newurl);
    $json = json_decode($address_info);
    $city = "";
    $state = "";
    $country = "";
    if (count($json->results) > 0) {
        //break up the components
        $arrComponents = $json->results[0]->address_components;

        foreach($arrComponents as $index=>$component) {
            $type = $component->types[0];

            if ($city == "" && ($type == "sublocality_level_1" || $type == "locality") ) {
                $city = trim($component->long_name);
            }
            if ($state == "" && $type=="administrative_area_level_1") {
                $state = trim($component->long_name);
            }
            if ($country == "" && $type=="country") {
                $country = trim($component->short_name);

                if ($blnUSA && $country!="US") {
                    $city = "";
                    $state = "";
                    break;
                }
            }
            if ($city != "" && $state != "" && $country != "") {
                //we're done
                break;
            }
        }
    }
    $arrReturn = array("city"=>$city, "state"=>$state);
    $finaldata = json_encode($arrReturn);

    $term_taxonomy = $wpdb->prefix."term_taxonomy";
	$terms = $wpdb->prefix."terms";
	$post_taxonomy = strtolower(post_taxonomy);
	$post_type = strtolower(post_type);

	$searchCount = $wpdb->get_var("SELECT COUNT(*) FROM $term_taxonomy WHERE `taxonomy` LIKE '$post_taxonomy'");

    if($searchCount > 0) {
		$get_srch_id = $wpdb->get_results ("SELECT * FROM $term_taxonomy WHERE `taxonomy` LIKE '$post_taxonomy'");
		/*foreach ($get_srch_id as $srch_id) {
			$check_srch_name = $wpdb->get_results ("SELECT * FROM $terms WHERE `term_id` = $srch_id->term_id AND `name` LIKE '%$zipcode%' ORDER BY `name` ASC ");
			foreach ($check_srch_name as $srch_name) {
				?>
					<div id="<?= trim($srch_name->term_id); ?>" class="state-guide col-sm-6" data-map="<?php echo $srch_name->name;?>" data-loop_index="2">
						<div class="state-data col-sm-12">
							<a href="<?= get_term_link($srch_name->name, $post_taxonomy); ?>">
								<div class="d-flex justify-content-between">
									<h5>
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevrons-right">
											<polyline points="13 17 18 12 13 7"></polyline>
											<polyline points="6 17 11 12 6 7"></polyline>
										</svg>
										<?php echo $srch_name->name;?>				
									</h5>
									<small> Last Updated 14 days ago </small>
								</div>
							</a>
							<p>
								<a href="#"> </a>
								<a class="badge badge-secondary" href="<?= get_term_link($srch_name->name, $post_taxonomy); ?>">
									<?php echo $srch_name->name;?>	
								</a>			
								<small class="text-muted">1 regions</small>
							</p>
						</div>
					</div>
				<?php
			}
		}*/
		foreach ($get_srch_id as $srch_id) {
			$taxonomy_name = $srch_id->taxonomy;
			$check_srch_name = $wpdb->get_results ("SELECT * FROM $terms WHERE `term_id` = $srch_id->term_id AND `name` LIKE '%$state%'ORDER BY `name` ASC ");
			foreach ($check_srch_name as $srch_name) {
				
				?>
					<div id="<?= trim($srch_name->term_id); ?>" class="state-guide col-sm-6" data-map="<?php echo $srch_name->name;?>" data-loop_index="2">
						<div class="state-data col-sm-12">
							<a href="<?= get_term_link($srch_name->name, $post_taxonomy); ?>">
								<div class="d-flex justify-content-between">
									<h5>
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevrons-right">
											<polyline points="13 17 18 12 13 7"></polyline>
											<polyline points="6 17 11 12 6 7"></polyline>
										</svg>
										<?php echo $srch_name->name;?>				
									</h5>
									<?php 
										$args = array(
											'posts_per_page' => 1,
										    'post_type' => str_replace(' ', '-', strtolower($post_type)),
										    'tax_query' => array(
										        array('taxonomy' => $taxonomy_name, 'field' => 'term_id', 'terms' => $srch_name->term_id)
										    )
										);
										$query = new WP_Query($args);
										if($query->have_posts()) {
											while($query->have_posts()) {
										        $query->the_post();
										        $updated_time = get_the_modified_time('h:i a');
												$updated_day = get_the_modified_time('F jS, Y');
										// .. or procedural style.
												$post_dt = date_create($updated_day. $updated_time);
												$current_dt = date_create(); // current date
												$interval = date_diff( $post_dt, $current_dt );

												if ($interval->format('%a') > 1) {
													$last_updated = 'Last Updated '. $interval->format('%a').' days ago';		
												} else {
													//
													if ($interval->format('%h') >= 1) {
														$last_updated = 'Last Updated '. $interval->format('%h').' hours ago';
													} else {
														$last_updated = 'Last Updated '. $interval->format('%i').' minutes ago';
													}
													
												}
										    }
										}
									?>
									<small> <?= $last_updated ?> </small>
								</div>
							</a>
							<p>
								<a href="#"> </a>
								<?php
									$child_index = 0;	
									$child_term = get_term_children($srch_id->term_id, $taxonomy_name );
									foreach ($child_term as $term_child) {
										if ($child_index < 4) {
											$city = get_term_by( 'id', $term_child, $taxonomy_name);					
											echo '<a class="badge badge-secondary" href="'.get_term_link($city->name, $taxonomy_name).'">'.ucfirst($city->name).'</a>';
										}
										$child_index++;
									}
								?>		
								<small class="text-muted"><?= count($child_term) ?> regions</small>
							</p>
						</div>
					</div>
				<?php
			}
		}
			
		?>  <?php
	} ?>
	<style type="text/css">
		.all_listing{
			width: 100%;
		}
	</style>
	<?php 
	wp_reset_postdata();  
    die();
}