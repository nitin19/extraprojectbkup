<?php
	function all_city_listing() {
		global $wpdb;
		$taxonomy_name = strtolower(post_taxonomy);
		$post_type = strtolower(post_type);
		?>
			<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
      		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
			<section class="guides">
				<?php include 'search_bar.php'; ?>
				<div class="row locations">
					<?php
						$terms_data = get_terms( array(
						    'taxonomy' => $taxonomy_name,
						    'hide_empty' => false,
						    'parent'	 => 0,
						    'orderby'    => 'name',
							'order'      => 'asc',							
						));
					?>
					<div id="regionsmap" class="row all_listing">
						<?php
							$loop_index == 0;
							foreach ($terms_data as $terms) {
								$term_id = $terms->term_id;
								$terms_name = $terms->name;
								$taxonomy_name = $terms->taxonomy;								
								?>
<div id="<?= $term_id; ?>" class="state-guide col-sm-6" 
	data-map="<?= $terms_name; ?>" data-loop_index="<?= $loop_index; ?>" >
	<div class="state-data col-sm-12">
		<a href="<?= get_term_link( $terms ); ?>">
			<div class="d-flex justify-content-between">
				<h5>
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevrons-right">
						<polyline points="13 17 18 12 13 7"></polyline>
						<polyline points="6 17 11 12 6 7"></polyline>
					</svg>
					<?= $terms->name; ?>
				</h5>
				<?php 
					$args = array(
						'posts_per_page' => 1,
					    'post_type' => str_replace(' ', '-', strtolower($post_type)),
					    'tax_query' => array(
					        array('taxonomy' => $taxonomy_name, 'field' => 'term_id', 'terms' => $term_id)
					    )
					);
					$query = new WP_Query($args);
					if($query->have_posts()) {
						while($query->have_posts()) {
					        $query->the_post();
					        $updated_time = get_the_modified_time('h:i a');
							$updated_day = get_the_modified_time('F jS, Y');
					// .. or procedural style.
							$post_dt = date_create($updated_day. $updated_time);
							$current_dt = date_create(); // current date
							$interval = date_diff( $post_dt, $current_dt );

							if ($interval->format('%a') > 1) {
								$last_updated = 'Last Updated '. $interval->format('%a').' days ago';		
							} else {
								//
								if ($interval->format('%h') >= 1) {
									$last_updated = 'Last Updated '. $interval->format('%h').' hours ago';
								} else {
									$last_updated = 'Last Updated '. $interval->format('%i').' minutes ago';
								}
								
							}
					    }
					}
				?>
				<small> <?= $last_updated ?> </small>
			</div>
		</a>
		<p>
			<a href="#"> </a>
			<?php
				$child_index = 0;
				$term_children = get_term_children($term_id, $taxonomy_name );
				foreach ($term_children as $term_child) {
					if ($child_index < 4) {
						$city = get_term_by( 'id', $term_child, $taxonomy_name);					
						echo '<a class="badge badge-secondary" href="'.get_term_link($city->name, $taxonomy_name).'">'.ucfirst($city->name).'</a>';
					}
					$child_index++;
				}
			?>
			<small class="text-muted"><?= count($term_children) ?> regions</small>
		</p>
	</div>
</div>
								<?php
								$loop_index++;
							}
						?>
					</div>						
				</div>
			</section>

		<?php
	}
