<?php	
	function single_taxonomy_template( $single_taxonomy ){
		if( is_tax(strtolower(post_taxonomy))){
			if(file_exists( CTGID_PATH .'/views/single_taxonomy_template.php')){
				return CTGID_PATH . '/views/single_taxonomy_template.php';
			}
		}
		return $single_taxonomy;
	}
	