<?php
	function single_city_guide($single) {
		global $post;
		$post_taxonomy = strtolower(post_taxonomy); 
		$post_type = strtolower(post_type);
		/* Checks for single template by post type */
		if($post->post_type == str_replace(' ', '-', strtolower($post_type))) {
			if(file_exists( CTGID_PATH .'/views/single_city_guide.php')){
				return CTGID_PATH . '/views/single_city_guide.php';
			}			
		}
		return $single;
	}