<?php
	include_once  '../../../../../wp-config.php';	
	global $wpdb;
		//	echo $wpdb->dbname;
	$term_taxonomy = $wpdb->prefix."term_taxonomy";
	$terms = $wpdb->prefix."terms";
	$post_taxonomy = strtolower(post_taxonomy);
	$post_type = strtolower(post_type);
	$srch = trim($_POST["keyword"]);

	$searchCount = $wpdb->get_var("SELECT COUNT(*) FROM $term_taxonomy WHERE `taxonomy` LIKE '$post_taxonomy'");

	if($searchCount > 0) {
		$get_srch_id = $wpdb->get_results ("SELECT * FROM $term_taxonomy WHERE `taxonomy` LIKE '$post_taxonomy'");
		$get_srch_pin = $wpdb->get_results ("SELECT * FROM $term_taxonomy WHERE `taxonomy` LIKE '$post_taxonomy' AND `description` LIKE '%$srch%'");

		?> <ul id="suggestion_list"> <?php
			foreach ($get_srch_id as $srch_id) {
				$check_srch_name = $wpdb->get_results ("SELECT * FROM $terms WHERE `term_id` = $srch_id->term_id AND `name` LIKE '%$srch%' ORDER BY `name` ASC ");
				foreach ($check_srch_name as $srch_name) {
					?>
						<li id="<?= trim($srch_name->term_id); ?>" class="selectTerm cursor capitalize" >
							<a href="<?= get_term_link($srch_name->name, $post_taxonomy); ?>">
								<?php echo trim($srch_name->name); ?>
							</a>
						</li>
					<?php
				}
			}
			if (!empty($get_srch_pin)) {
				foreach ($get_srch_pin as $srch_pin) {
					$get_srch_name = $wpdb->get_results ("SELECT * FROM $terms WHERE `term_id` = $srch_pin->term_id");

					$city_pin = explode(', ', $srch_pin->description);
					foreach ($city_pin as $pincode) {					
						if (!empty($srch)) {
							if (strpos($pincode, $srch) != false) {
								?>
									<li id="<?= trim($srch_pin->term_id); ?>" class="selectTerm cursor capitalize" >
										<a href="<?= get_term_link($get_srch_name[0]->name, $post_taxonomy); ?>">
											<?php echo $pincode ?>
										</a>
									</li>
								<?php
							}
						} else {
							?>
								<li id="<?= trim($srch_pin->term_id); ?>" class="selectTerm cursor capitalize" >
									<a href="<?= get_term_link($get_srch_name[0]->name, $post_taxonomy); ?>">
										<?php echo $pincode ?>
									</a>
								</li>
							<?php
						}
					}
				}
			}
		?> </ul> <?php
	}
?>