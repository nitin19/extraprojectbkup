<?php
	function CTGID_style_n_scripts() {
		wp_enqueue_style('CTGID_bootstrap_css', CTGID_URL.'assist/bootstrap-4.4.1/css/bootstrap.min.css', array(), CTGID_VERSION, 'all' );
		wp_enqueue_style('CTGID_style', CTGID_URL.'assist/style.css', array(), CTGID_VERSION, 'all' );
		wp_enqueue_style('CTGID_font-awesome', CTGID_URL.'assist/font-awesome/font-awesome.min.css', array(), CTGID_VERSION, 'all' );
		
		// wp_enqueue_style('CTGID_slick', CTGID_URL.'assist/slick/slick.css', array(), CTGID_VERSION, 'all' );
		// wp_enqueue_style('CTGID_slick_theme', CTGID_URL.'assist/slick/slick-theme.css', array(), CTGID_VERSION, 'all' );

		wp_enqueue_script('CTGID_jquery_2_24', CTGID_URL.'assist/js/jquery-2.2.4.min.js', array('jquery'), CTGID_VERSION, true );
		//wp_enqueue_script('slick_js', CTGID_URL.'assist/slick/slick.js', array('jquery'), CTGID_VERSION, true );

		wp_enqueue_script('CTGID_jquery_validate', CTGID_URL.'assist/js/jquery.validate.min.js', array('jquery'), CTGID_VERSION, true );	

		wp_enqueue_script('CTGID_custom_js', CTGID_URL.'assist/myCustomJs.js', array('jquery'), CTGID_VERSION, true );	
	}