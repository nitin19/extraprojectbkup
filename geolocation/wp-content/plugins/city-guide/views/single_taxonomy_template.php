<?php get_header(); ?>
    <!-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css"> -->
    <section class="guides">
        <?php include CTGID_PATH . '/includes/templates/search_bar.php'; ?>
        <div class="locations">
            <div class="row all_city_listing">
                <?php 
                    //get_header();
                    $term = $wp_query->get_queried_object();
                    $term_id = $term->term_id;
                    $taxonomy_name = $term->taxonomy;
                    $term_children = get_term_children( $term_id, $taxonomy_name );
                    $post_taxonomy = strtolower(post_taxonomy); 
                    $post_type = strtolower(post_type);

                    if($term_children){
                        foreach($term_children as $child) {
                            $city = get_term_by( 'id', $child, $taxonomy_name );
                            ?>
                                <div id="term_<?= $city->term_id; ?>" class="col-sm-3 terms-block city">
                                    <div class="row term_heading">
                                        <div class="col-sm-12 col-xs-12">
                                            <a href="<?= get_term_link($city->name, $taxonomy_name); ?>"><?= ucfirst($city->name); ?></a>
                                        </div>
                                        <!-- <div class="col-sm-1 col-xs-1">
                                            <span class="togle_btn" data-id="<?= $cityData; ?>" > + </span>
                                        </div> -->
                                    </div>
                                </div>
                            <?php
                        }
                    } else {
                        $args = array(
                            'post_type' => str_replace( ' ', '-', strtolower($post_type)),
                            'tax_query' => array(
                                array('taxonomy' => $post_taxonomy, 'field'    => 'term_id', 'terms'    => $term_id)
                            )
                        );
                        $query = new WP_Query($args);
                        if($query->have_posts()) {
                            ?>
                                <ul class="city_guide_list">
                                    <?php
                                        while($query->have_posts()) {
                                            $query->the_post();
                                            ?>
                                                <li> <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a> </li>
                                            <?php
                                        }
                                    ?>
                                </ul>
                            <?php
                        } else {
                            echo 'no post';
                        }
                    }
                ?>
            </div>
        </div>
    </section>
    <?php
get_footer();







