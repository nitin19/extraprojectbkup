<div class="container">
	<div class="row section">
		<?php 
			$terms_data = get_terms( array(
			    'taxonomy' => 'state',
			    'hide_empty' => false,
			));
			foreach ($terms_data as $terms) {
				?>
					<div id="term_<?= $terms->term_id; ?>" class="col-sm-3 terms-block state">
						<div class="row term_heading">
							<div class="col-sm-10 col-xs-10">
								<a href="<?= get_term_link( $terms ); ?>"><?= $terms->name; ?></a>	
							</div>
							<div class="col-sm-1 col-xs-1">
								<span class="togle_btn" data-id="<?= $terms->term_id; ?>"> + </span>
							</div>	
						</div>
						
					</div>
				<?php							
			}

		?>	
	</div>
</div>