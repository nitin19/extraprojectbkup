<?php
	function city_guide_page() {
		global $wpdb;
		$page = get_page_by_title( 'City Guide', OBJECT, 'page' );
		$page_id = null == $page ? -1 : $page->ID;
		if( -1 == $page_id ) {
			global $user_ID;
			$new_post = array(
				'post_title' 	=> 'City Guide',
				'post_content' 	=> '[wp_all_city_listing]',
				'post_status'	=> 'publish',
				'post_type'		=> 'page',
				'post_author' 	=> 1
			);
			$post_id = wp_insert_post($new_post);
		}		
	}