<?php
	function city_register_post_type() {
		$singular = post_type;
		$plural = post_type;
		$slug = str_replace( ' ', '-', strtolower($singular));
		$labels = array(
			'name' 					=> $plural,
			'singular_name' 		=> $singular,		
			'all_items'         	=> 'All ' . $singular, 		
			'menu_name'         	=> 'Citys Guide ',  'Admin menu name' . $singular,		
			'add_new' 				=> 'Add New',		
			'add_new_item'  		=> 'Add New ' . $singular,		
			'edit'		        	=> 'Edit',		
			'edit_item'	        	=> 'Edit ' . $singular,		
			'new_item'	     		=> 'New ' . $singular,		
			'view' 					=> 'View ' . $singular,		
			'view_item' 			=> 'View ' . $singular,		
			'search_term' 		  	=> 'Search ' . $plural,		
			'parent' 				=> 'Parent ' . $singular,
			'featured_image'    	=> 'Set '. $singular.' Cover Image ', 
			'set_featured_image'    => 'Set '. $singular .' image ', 
			'remove_featured_image' => 'Remove '. $singular .' image ', 
			'use_featured_image'    => 'Use as '. $singular .' image ',  
			'not_found' 			=> 'No ' . $plural .' found',		
			'not_found_in_trash' 	=> 'No ' . $plural .' in Trash',
			'insert_into_item'      => 'Insert into ' . $singular, 
			'uploaded_to_this_item' => 'Uploaded to this  ' . $singular,
			'filter_items_list'     => 'Filter' . $plural,
			'items_list_navigation' => $plural. ' navigation ' . $singular,
			'items_list'            => $plural. ' list'
		);
		$args = array(
			'labels'              => $labels,
	        'public'              => true,
	        'publicly_queryable'  => true,
	        'exclude_from_search' => false,
	        'show_in_nav_menus'   => true,
	        'show_ui'             => true,
	        'show_in_menu'        => true,
	        'show_in_admin_bar'   => true,
	        'menu_position'       => 10,
	        'menu_icon'           => 'dashicons-location-alt',
	        'can_export'          => true,
	        'delete_with_user'    => false,
	        'hierarchical'        => false,
	        'has_archive'         => true,
	        'query_var'           => true,
	        'capability_type'     => 'post',
	        'map_meta_cap'        => true,
	        // 'capabilities' => array(),
	        'rewrite'             => array(
	        	'slug' 			=> $slug,
	        	'with_front' 	=> true,
	        	'pages' 		=> true,
	        	'feeds' 		=> true,
	        ),
	        'supports'            => array( 
	        	'title', 'editor', 'thumbnail', 'excerpt','tag', 'comments', 'author', 'rating', 'publicize'
	        )
	    );
	    register_post_type( $slug, $args );
	    flush_rewrite_rules();
	}
