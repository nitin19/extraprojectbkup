<?php

    function city_guide_state() {
        $postType = str_replace( ' ', '-', strtolower(post_type));
        
        $type_singular = post_taxonomy;
        $type_plural = post_taxonomy;
        $type_slug = str_replace( ' ', '_', strtolower($type_singular) );
        $type_labels = array(
            'name'                       => $type_plural,
            'singular_name'              => $type_singular,
            'search_items'               => 'Search ' . $type_plural,
            'popular_items'              => 'Popular ' . $type_plural,
            'all_items'                  => 'All ' . $type_plural,
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => 'Edit ' . $type_singular,
            'update_item'                => 'Update ' . $type_singular,
            'add_new_item'               => 'Add New ' . $type_singular,
            'new_item_name'              => 'New ' . $type_singular . ' Name',
            'separate_items_with_commas' => 'Separate ' . $type_plural . ' with commas',
            'add_or_remove_items'        => 'Add or remove ' . $type_plural,
            'choose_from_most_used'      => 'Choose from the most used ' . $type_plural,
            'not_found'                  => 'No ' . $type_plural . ' found.',
            'menu_name'                  => $type_plural,
        );
        $type_args = array(
            'hierarchical'          => true,
            'labels'                => $type_labels,
            'show_ui'               => true,
            'show_admin_column'     => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var'             => true,
            'rewrite'               => array( 'slug' => $type_slug )
        );
        
        register_taxonomy( $type_slug, $postType, $type_args );
    }
    // function city_guide_city() {
    //     $type_singular = 'City';
    //     $type_plural = 'Cities';
    //     $type_slug = str_replace( ' ', '_', strtolower($type_singular) );
    //     $type_labels = array(
    //         'name'                       => $type_plural,
    //         'singular_name'              => $type_singular,
    //         'search_items'               => 'Search ' . $type_plural,
    //         'popular_items'              => 'Popular ' . $type_plural,
    //         'all_items'                  => 'All ' . $type_plural,
    //         'parent_item'                => null,
    //         'parent_item_colon'          => null,
    //         'edit_item'                  => 'Edit ' . $type_singular,
    //         'update_item'                => 'Update ' . $type_singular,
    //         'add_new_item'               => 'Add New ' . $type_singular,
    //         'new_item_name'              => 'New ' . $type_singular . ' Name',
    //         'separate_items_with_commas' => 'Separate ' . $type_plural . ' with commas',
    //         'add_or_remove_items'        => 'Add or remove ' . $type_plural,
    //         'choose_from_most_used'      => 'Choose from the most used ' . $type_plural,
    //         'not_found'                  => 'No ' . $type_plural . ' found.',
    //         'menu_name'                  => $type_plural,
    //     );
    //     $type_args = array(
    //         'hierarchical'          => true,
    //         'labels'                => $type_labels,
    //         'show_ui'               => true,
    //         'show_admin_column'     => true,
    //         'update_count_callback' => '_update_post_term_count',
    //         'query_var'             => true,
    //         'rewrite'               => array( 'slug' => $type_slug )
    //     );
    //     register_taxonomy( $type_slug, 'city-guide', $type_args );
    // }
    // function city_guide_zipcode() {
    //     $type_singular = 'Zipcode';
    //     $type_plural = 'Zipcode';
    //     $type_slug = str_replace( ' ', '_', strtolower($type_singular) );
    //     $type_labels = array(
    //         'name'                       => $type_plural,
    //         'singular_name'              => $type_singular,
    //         'search_items'               => 'Search ' . $type_plural,
    //         'popular_items'              => 'Popular ' . $type_plural,
    //         'all_items'                  => 'All ' . $type_plural,
    //         'parent_item'                => null,
    //         'parent_item_colon'          => null,
    //         'edit_item'                  => 'Edit ' . $type_singular,
    //         'update_item'                => 'Update ' . $type_singular,
    //         'add_new_item'               => 'Add New ' . $type_singular,
    //         'new_item_name'              => 'New ' . $type_singular . ' Name',
    //         'separate_items_with_commas' => 'Separate ' . $type_plural . ' with commas',
    //         'add_or_remove_items'        => 'Add or remove ' . $type_plural,
    //         'choose_from_most_used'      => 'Choose from the most used ' . $type_plural,
    //         'not_found'                  => 'No ' . $type_plural . ' found.',
    //         'menu_name'                  => $type_plural,
    //     );
    //     $type_args = array(
    //         'hierarchical'          => true,
    //         'labels'                => $type_labels,
    //         'show_ui'               => true,
    //         'show_admin_column'     => true,
    //         'update_count_callback' => '_update_post_term_count',
    //         'query_var'             => true,
    //         'rewrite'               => array( 'slug' => $type_slug )
    //     );
    //     register_taxonomy( $type_slug, 'city-guide', $type_args );
    // }    

    