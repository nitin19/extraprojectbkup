<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'geolocation' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'YJHSNZ7WFoC~jx#zxeAi(Zp7J{.9mCLyqfV@)%/TTZWnI> h%X]teZ7m(`!Tt]OB' );
define( 'SECURE_AUTH_KEY',  'B!J kJwFe`(D5/=19gB}dR[8<K7;|fTPZnIkJWkjDIT3,?UUkyM$5BBg9,1ugl1u' );
define( 'LOGGED_IN_KEY',    'bAX?vAG>ghBS$HhKnO;DjUkJnrcmJ-TZ[WX]2D_]s-u68nse_Bo-7vRm{5[bD1w+' );
define( 'NONCE_KEY',        'atK(+K?L[g6$FrFLb%)ahL~yx3N6QWz2B;t{^T2?2df=[ODIYY%O?tT8ZPKWO|/m' );
define( 'AUTH_SALT',        '%](Fr5t@e.MZyN0_)g{E/PKy%kc046A/.;=7tU(bjt/Ua,#v9!L<i*G0X_5@zSJq' );
define( 'SECURE_AUTH_SALT', 'dS,u0pZq#a+|WV$< KCg7eV;RUW(h|yJfvc)=-pw%oU# Aja7*}~aA6Vju8tROt4' );
define( 'LOGGED_IN_SALT',   '{roCNS;<+#jog t{zX0Q=Q4_Qt$#gtZ/8pV5KaB3#J_Un7&wg*wLdfki1mE;$<TJ' );
define( 'NONCE_SALT',       'xnv0 YwmSKQbLK-KzhAk[q#j)& *Wgzqt=,HPy<4Mf0U|I1:d$|E*3{dz=1_2N+L' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define('FS_METHOD', 'direct');
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
